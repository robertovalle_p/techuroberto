import {LitElement,html} from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';
class PersonaMain extends LitElement {

static get properties(){
    return {
        people: {type: Array},
        showPersonForm: {type: Boolean}
    };
}

constructor () {
    super();


    this.people = [
        {
            name: "Ellen Ripley",
            yearsInCompany: 5,
            photo: {
                "src": "./img/persona.jpg",
                "alt": "Ellen Ripley"
            },
            profile: "loren ipsum dlor sit amet."
        },{
            name:"Bruce Banner",
            yearsInCompany: 2,
            photo: {
            "src": "./img/persona.jpg",
            "alt": "Bruce Banner"
            },
            profile: "loren ipsum dlor sit amet 1."
        },{  
            name: "Eowyn",
            yearsInCompany: 3,
            photo: {
            "src": "./img/persona.jpg",
            "alt": "Eowyn"
            },
            profile: "loren ipsum dlor sit amet2 ajajajaj aj jjaj juhujhi    jajajajj  ."
        },{  
            name: "Turlanga Leela",
            yearsInCompany: 9,
            photo: {
            "src": "./img/persona.jpg",
            "alt": "Turlanga Leela"
            },
            profile: "loren ipsum dlor sit amet3."
        },{  
            name: "Tyrion Lannister",
            yearsInCompany: 9,
            photo: {
            "src": "./img/persona.jpg",
            "alt": "Tyrion Lannister"
            },
            profile: "loren ipsum dlor sit amet4."
        }
    
    ];

       this.showPersonForm = false;

    }
        updated(changedProperties) {
            console.log("update");

            if(changedProperties.has("showPersonForm")) {

                console.log("ha cambiado el valor de la propiedad show personaform en persona-main");

                if (this.showPersonForm === true) {
                    this.showPersonFormData();
                }    else {
                        this.showPersonList();
                }
            } 
// si ha cambiado el array se cambia en el modo de dart de alta una persona para que se detecte el cambio
            if (changedProperties.has("people")){
                console.log("ha cambiado el valor de la propiedad people en  persona-main");

                //lanzamos el evento con el array hacia persona APP
                this.dispatchEvent(
                    new CustomEvent("updated-people", {
                        detail: { people: this.people }
                    }
                    )
                );
            }
        }

        showPersonList(){
            console.log("showPersonList")
            console.log("mostrando listado de personas")
            this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
            this.shadowRoot.getElementById("personForm").classList.add("d-none");
        }

        showPersonFormData(){
            console.log("showPersonFormData")
            console.log("mostrar el formulario de persona")
            this.shadowRoot.getElementById("peopleList").classList.add("d-none");
            this.shadowRoot.getElementById("personForm").classList.remove("d-none");
        }



    render() {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <h2 class="text-center"">Personas</h2>
        <div class="row" id="peopleList">
            <div class="row row-cols-1 row-cols-sm-4">
                ${this.people.map(
                    person => html`<persona-ficha-listado 
                                @delete-person="${this.deletePerson}"
                                @info-person="${this.infoPerson}"
                                fname="${person.name}"
                                yearsInCompany="${person.yearsInCompany}"
                                profile="${person.profile}"
                                .photo="${person.photo}"
                                >
                        </persona-ficha-listado>`
                )}
            </div>
        </div>


         <div class="row">
                    <persona-form  
                        @persona-form-store="${this.personFormStore}"
                        @persona-form-close="${this.personFormClose}"
                        id="personForm" 
                        class="d-none border rounded border-primary">
                    </persona-form>   
        </div>
     

        `;
    }

    deletePerson(e){

        console.log("deletePerson en persona-main"); 
        console.log("me piden borrar " + e.detail.name); 
        console.log("personas antes de borrar  " ); 
        console.log(this.people); 

        this.people =this.people.filter(
             person => person.name != e.detail.name     
        );

        //forzamos la actualizacion y que vuelva a pasar por el render. 
        // this.requestUpdate();

        console.log("personas despues de borrar  " ); 
        console.log(this.people); 

    }

    infoPerson(e) {
        console.log(" infoperson en persona main");
        console.log(" se ha pedido informacion de la persona " + e.detail.name);

        // filter devuelve un array
        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );

      //  console.log(chosenPerson);

      // trabajo con el cambio de propiedades. ponemos [0] por ser la primera posicion del array
        this.shadowRoot.getElementById("personForm").person = chosenPerson[0];
        this.shadowRoot.getElementById("personForm").editingPerson = true;
      // activimaos el formulario paraq ue se muestree.  
        this.showPersonForm = true;
    }


    personFormStore(e) {
        console.log("personFormStore  " ); 
        console.log("se va a almacenar una persona"); 

        console.log(e.detail.person); 
        
        if(e.detail.editingPerson === true)  {
            console.log("se va a actualizar una persona" + e.detail.person.name); 

            this.people = this.people.map(
                person => person.name === e.detail.person.name     
                //hacemos un ternario si es verdadero ejecuta lo de la izq si no lo de la derecha
                   ? person = e.detail.person : person
            );

// sustituimos por las lineas de arriba para que puda identificarse el cambio en el array
//             let indexOfPerson = 
//                 this.people.findIndex(
//                     person => person.name === e.detail.person.name
//                 );
// // ponemos el array original ihual a lo que nos han modificado
//             if (indexOfPerson >= 0 ){
//                 console.log("persona encontrada");
//                     this.people[indexOfPerson] = e.detail.person;
//             }


        } else {
            console.log("se va a almacenar una persona" + e.detail.person.name); 
            this.people.push(e.detail.person);

            // esto es igual al array enteroi con los tres puntos... es como descomponer el array
            //original y el ultimo elemento es el nuevo ahora si detecta el cambio del array

            this.people = [...this.people, e.detail.person];

            console.log("persona almacenada"); 

        }

        console.log("Proceso terminado"); 



        this.showPersonForm = false;

    }

    personFormClose(e) {
        console.log("personFormClose  " ); 
        console.log("se ha cerrado el formulario de persona"); 

        this.showPersonForm = false;
    }

    
}

customElements.define('persona-main',PersonaMain)