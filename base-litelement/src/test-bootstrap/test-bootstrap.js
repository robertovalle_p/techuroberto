import {LitElement,html,css } from 'lit-element';

class TestBootstrap extends LitElement {



    static get styles(){
        return css`
        .redbg{
            background-color: red;
        }
        .greenbg{
            background-color: green;
        }
        .bluebg{
            background-color: blue;
        }
        .greybg{
            background-color: grey;
        }
        `;
    }

static get properties(){
    return {
    };
}


constructor () {
    super();
}

    render() {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <h3> Test Bootstrap </h3>
        <div class="row greybg">
            <div class="col-2 offset-1 redbg"> col 1</div> 
            <div class="col-3 offset-2 greenbg"> col 1</div> 
            <div class="col-4 bluebg"> col 1</div> 
        </div> 
        `;
    }
}

customElements.define('test-bootstrap',TestBootstrap)