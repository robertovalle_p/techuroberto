import {LitElement,html} from 'lit-element';
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';
import '../persona-sidebar/persona-sidebar.js';
import '../persona-stats/persona-stats.js';

class PersonaApp extends LitElement {



static get properties(){
    return {
        people:{type: Array}
    };
}

constructor () {
    super();

    // this.people = [];
}


//@updated-people recuperamos el evento que viene desde persona-stats y previamente de main
    render() {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <persona-header></persona-header>
        <div class="row">
            <persona-sidebar @new-person="${this.newPerson}" class="col-2"></persona-sidebar>
            <persona-main @updated-people=${this.updatePeople}" class="col-10"></persona-main>
        </div>

        <persona-footer></persona-footer>
        <persona-stats @updated-people-stats="${this.peopleStatsUpdated}"></persona-stats>
        `;
    }

    newPerson(e){
        console.log("new person en persona app");
        this.shadowRoot.querySelector("persona-main").showPersonForm = true;
    }

    updatePeople(e) {
         console.log("update people en  person en persona app");
         console.log(e.detail.people);
         this.people = e.detail.people;

    }
// llega la informacion desde stats via @updated-people-stats
    peopleStatsUpdated(e) {
        console.log("peopleStatsUpdated");
        //con esto mandamos los datos a persona sidebar para que los utilice
        this.shadowRoot.querySelector("persona-sidebar").peopleStats = e.detail.peopleStats;

    }


    updated(changedProperties) {
        console.log("updated");

        if (changedProperties.has("people")) {
            console.log("ha cambniado el valor de la propiedad people en  person en persona app");

            //guardamos la informacion del array para mandarselo a persona-stats
            this.shadowRoot.querySelector("persona-stats").people = this.people;

        }
    }
}

customElements.define('persona-app',PersonaApp)