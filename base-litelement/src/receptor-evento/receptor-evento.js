import {LitElement,html} from 'lit-element';

class ReceptorEvento extends LitElement {


static get properties(){

    return{
        course:{type: String},
        year:{type: String}
    }
}

    render() {
        return html`
        <h3>Receptor Evento</h3>
        <h5> Este curso ${this.course}</h5>
        <h5> Este curso ${this.year}</h5>
        `;
    }
}

customElements.define('receptor-evento',ReceptorEvento)